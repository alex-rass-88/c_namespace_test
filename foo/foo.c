#include "foo.h"
#include <stdio.h>

int my_bar(int a, char* s) {
  printf("foo.bar(%d, %s)\n", a, s);
  return a;
}

void my_baz(void) { printf("%s\n", "foo.baz()"); }

foo_api const foo = {
    my_bar,
    my_baz,
};
