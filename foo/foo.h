#ifndef FOO_H
#define FOO_H

#if !defined(_EXPORT_)
#if defined(_WIN32) || defined(_WIN64)
#ifdef _BUILD_SHARED
#define _EXPORT_ __declspec(dllexport)
#else  // _BUILD_SHARED
#define _EXPORT_ __declspec(dllimport)
#endif
#else  // _WIN32
#ifdef _BUILD_SHARED
#define _EXPORT_ __attribute__((visibility("default")))
#else  // _BUILD_SHARED
#define _EXPORT_
#endif  // _BUILD_SHARED
#endif  // _WIN32
#endif  // _EXPORT_

typedef struct {
  int (*const bar)(int, char*);
  void (*const baz)(void);
} foo_api;

extern _EXPORT_ foo_api const foo;

#endif  // FOO_H
